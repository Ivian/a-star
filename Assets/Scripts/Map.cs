﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Priority_Queue;
using System.Linq;

public class Map : MonoBehaviour
{
	public const int mapWidth = 20;
	public const int mapHeight = 20;

	public const float tileWidth = 96f;
	public const float tileHeight = 96f;

	public const float horizontalSpace = 10f;
	public const float verticalSpace = 10f;

	public const float pixelsPerUnit = 100f;


	public const float travelTime = 0.2f;

	private Field[,] map;

	private Field playerField = null;

	public static Map instance;

	internal void FieldSelected(Field field)
	{
		var path = FindPath(field);
		StartCoroutine(ProcessPath(path));
	}

	private IEnumerator ProcessPath(Stack<Field> path)
	{
		while (path.Count > 0)
		{
			if (playerField != null)
				playerField.PlayerLeftTile();
			playerField = path.Pop();


			playerField.PlayerEnnteredTile();

			var f = GetField(playerField.pointInArray.Offset(1, 0));
			if (f != null) f.RevealTile();

			f = GetField(playerField.pointInArray.Offset(-1, 0));
			if (f != null) f.RevealTile();

			f = GetField(playerField.pointInArray.Offset(0, -1));
			if (f != null) f.RevealTile();

			f = GetField(playerField.pointInArray.Offset(0, 1));
			if (f != null) f.RevealTile();

			yield return new WaitForSeconds(travelTime);
		}
	}

	private void Awake()
	{
		if (instance == null) instance = this;
		else Destroy(this);
	}

	private void Start()
	{
		GenerateMap();
	}

	private void GenerateMap()
	{
		var tileResource = Resources.Load<GameObject>("Tile");

		map = new Field[mapHeight, mapWidth];

		for (int y = 0; y < mapHeight; y++)
		{
			var y0 = -(mapHeight / 2) + y;
			for (int x = 0; x < mapWidth; x++)
			{
				var x0 = -(mapWidth / 2) + x;

				var tileGo = Instantiate(tileResource);
				var tileTrans = tileGo.GetComponent<Transform>();

				tileTrans.SetParent(transform);
				tileTrans.localPosition = new Vector3(x0 * (tileWidth + horizontalSpace) / pixelsPerUnit, y0 * (tileHeight + verticalSpace) / pixelsPerUnit, 0f);
				tileTrans.localScale = Vector3.one;

				var tile = tileGo.GetComponent<Field>();
				tile.pointInArray = new Point(x, y);
				tile.coord = new Point(x0, y0);
				tile.walkability.SetFlag(FieldState.Hidden);
				tile.SetDefaultColor();
				//	if (UnityEngine.Random.Range(0f, 100f) < 3f) tile.walkability.SetFlag(FieldState.Untraceable);

				map[y, x] = tile;
			}
		}
	}

	/// <summary>
	/// Get walkability value at grid point
	/// </summary>
	/// <param name="arrayPoint">Grid point</param>
	/// <returns>walkability value</returns>
	public Field GetField(Point arrayPoint)
	{
		if (arrayPoint.y >= mapHeight || arrayPoint.x >= mapWidth) return null;
		if (arrayPoint.y < 0 || arrayPoint.x < 0) return null;
		return map[arrayPoint.y, arrayPoint.x];
	}

	/// <summary>
	/// Get walkability value at grid point
	/// </summary>
	/// <param name="arrayPoint">Grid point</param>
	/// <returns>walkability value</returns>
	public FlagMask<FieldState> GetWalkability(Point arrayPoint)
	{
		var field = GetField(arrayPoint);
		if (field == null) return new FlagMask<FieldState>(FieldState.Invalid);
		return field.walkability;
	}

	/// <summary>
	/// Set walkability value at grid point
	/// </summary>
	/// <param name="p">Grid point</param>
	/// <param name="value">value to set to</param>
	public void AddWalkabilityValue(Field field, FieldState value)
	{
		var p = field.pointInArray;
		if (p.y >= mapHeight || p.x >= mapWidth) return;
		if (p.y < 0 || p.x < 0) return;
		field.walkability.SetFlag(value);
	}

	/// <summary>
	/// Set walkability value at grid point
	/// </summary>
	/// <param name="p">Grid point</param>
	/// <param name="value">value to set to</param>
	public void UnsetWalkabilityValue(Field field, FieldState value)
	{
		var p = field.pointInArray;
		if (p.y >= mapHeight || p.x >= mapWidth) return;
		if (p.y < 0 || p.x < 0) return;
		field.walkability.UnsetFlag(value);
	}

	private void SearchNeighbour(Field targetGrid,
		HeapPriorityQueue<MapSearchNode> openNodes,
		MapSearchNode searchRootNode,
		Point searchedPoint,
		bool travelingToHidden)
	{
		var state = GetWalkability(searchedPoint);
		// If searched node is not on closed list, is not unwalkable and is not invalid
		if (state.IsSetAny(FieldState.WalkableClosed, FieldState.WalkableOpen, (travelingToHidden && state.IsSet(FieldState.Hidden)) ? FieldState.Invalid : FieldState.Unwalkable, FieldState.Invalid) == false)
		{
			var field = GetField(searchedPoint);
			// Add it to open nodes
			openNodes.Enqueue(MapSearchNode.Create(field, targetGrid, searchRootNode));
			AddWalkabilityValue(field, FieldState.WalkableOpen);
		}
	}

	private void ResetWalkability(FlagMask<FieldState> currentWalkability, bool tracingToUnwalkable)
	{
		// Revert the walkability change in target grid
		if (tracingToUnwalkable)
		{
			currentWalkability.SetFlag(FieldState.Unwalkable);
		}

		// Revert all walkabilityu process flags
		for (int y = 0; y < mapHeight; y++)
		{
			for (int x = 0; x < mapWidth; x++)
			{
				var f = GetField(new Point(x, y));
				if (f == null) continue;
				f.walkability.UnsetFlag(FieldState.WalkableClosed);
				f.walkability.UnsetFlag(FieldState.WalkableOpen);
			}
		}
	}

	public Stack<Field> FindPath(Field targetField)
	{
		if (playerField == null) return new Stack<Field>(targetField.Yield());
		if (playerField == targetField) { Debug.LogWarning("Pathfinding: Requested desttination is the same as start position"); return null; }

		bool tracingToUnwalkable = false;
		bool tracingToHiddedn = false;

		if (targetField.walkability.IsSet(FieldState.Hidden))  // Tracing to hidden location is diffrent
		{
			Debug.Log("Pathfinding: Tracing to unrevealed location");
			tracingToHiddedn = true;
		}
		else        // Tracing to visible location
		{
			// Discard tracing to untraceable visible location
			if (targetField.walkability.IsSet(FieldState.Untraceable)) { Debug.LogWarning("Pathfinding: Requested destination can't be resolved as destination"); return null; }

			// Accept tracing to traceable, but unwalkable visible location
			if (targetField.walkability.IsSet(FieldState.Unwalkable))
			{
				Debug.Log("Pathfinding: Requested destination is not walkable, but can be resolved as destination");
				targetField.walkability.UnsetFlag(FieldState.Unwalkable);
				tracingToUnwalkable = true;
			}
		}

		var closedNodes = new List<MapSearchNode>();
		var openNodes = new HeapPriorityQueue<MapSearchNode>(500);   // change dat value

		var startNode = MapSearchNode.Create(playerField, targetField, null, MapSearchNode.NodeType.Start);

		// Add to open list start node.
		openNodes.Enqueue(startNode);
		AddWalkabilityValue(startNode.point, FieldState.WalkableOpen);

		MapSearchNode destinationNode = null;
		var targetGrid = targetField.pointInArray;

		do
		{
			if (openNodes.Count == 0) // End of possible ways to go.
			{
				Debug.LogWarning("Pathfinding: The requested location is unreachable!");

				if (tracingToHiddedn)   // If tracing to hidden location try to find closest tile to destination
				{
					double lowestEstimatedCost = closedNodes.Select(t => t.estimatedTargetCost).Min();

					var lowestEstCostArray = closedNodes.Where(t => Math.Abs(t.estimatedTargetCost - lowestEstimatedCost) < float.Epsilon).ToList();
					double lowsetPathCost = closedNodes.Where(t => t.estimatedTargetCost == lowestEstimatedCost).ToList().Select(k => k.realPathCost).Min();
					destinationNode = lowestEstCostArray.Where(t => t.realPathCost == lowsetPathCost).ToList()[0];

					break;
				}
				else // Else finish tracing, and return null as path
				{
					ResetWalkability(targetField.walkability, tracingToUnwalkable);
					return null;
				}
			}

			// Remove from openlist the less est. cost element
			var rootNode = openNodes.Dequeue();
			if (tracingToHiddedn && rootNode.point.walkability.IsSet(FieldState.Unwalkable) && rootNode.point.walkability.IsSet(FieldState.Hidden))
				break;

			// Add it to "closed" list
			closedNodes.Add(rootNode);
			// And set it's property on map
			AddWalkabilityValue(rootNode.point, FieldState.WalkableClosed);

			var p = rootNode.point.pointInArray;

			// Search in four neighbour cells
			SearchNeighbour(targetField, openNodes, rootNode, p.Offset(0, 1), tracingToHiddedn);        // 1 right
			SearchNeighbour(targetField, openNodes, rootNode, p.Offset(0, -1), tracingToHiddedn);       // 1 left
			SearchNeighbour(targetField, openNodes, rootNode, p.Offset(1, 0), tracingToHiddedn);        // 1 up
			SearchNeighbour(targetField, openNodes, rootNode, p.Offset(-1, 0), tracingToHiddedn);       // 1 down
		}
		while (closedNodes[closedNodes.Count - 1].point.pointInArray.x != targetGrid.x || closedNodes[closedNodes.Count - 1].point.pointInArray.y != targetGrid.y);

		var path = new Stack<Field>();

		if (destinationNode == null)
			destinationNode = closedNodes[closedNodes.Count - 1];

		// Build path
		while (destinationNode.parent != null)
		{
			path.Push(destinationNode.point);// Add(n.point);
			destinationNode = destinationNode.parent;
		}

		ResetWalkability(targetField.walkability, tracingToUnwalkable);

		return path;
	}
}