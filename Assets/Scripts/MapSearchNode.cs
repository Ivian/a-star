﻿using Priority_Queue;
using UnityEngine;

public class MapSearchNode : PriorityQueueNode
{
	public enum NodeType
	{
		Open,
		Closed,
		Start,
		Target
	}

	/// <summary>
	/// Cost of traveling through all tiles from start to this node
	/// It's a sum of realCost of parent, and cost of moving
	/// from parent to this node
	/// </summary>
	public double realPathCost { get { if (parent != null) return parent.realPathCost + moveCostToDirectNeighbour; return 0; } }

	/// <summary>
	/// Heuristic estimated cost to target
	/// </summary>
	public double estimatedTargetCost;

	/// <summary>
	/// Final heuristic cost of path using path created up to this node
	/// </summary>
	public double finalCost { get { return realPathCost + estimatedTargetCost; } }

	public const double moveCostToDirectNeighbour = 1;
	public const double moveCostToDiagonalNeighbour = 1.47;

	/// <summary>
	/// Calculate estimated cost to target
	/// </summary>
	/// <param name="target"></param>
	public void SetEstimatedCost(Field target)
	{
		estimatedTargetCost = 0; // reset

		for (int x = 0; x < Mathf.Abs(target.pointInArray.x - point.pointInArray.x); x++)
			estimatedTargetCost += moveCostToDirectNeighbour;

		for (int y = 0; y < Mathf.Abs(target.pointInArray.y - point.pointInArray.y); y++)
			estimatedTargetCost += moveCostToDirectNeighbour;
	}

	/// <summary>
	/// Parent node
	/// </summary>
	public MapSearchNode parent;

	/// <summary>
	/// Type of node
	/// </summary>
	public NodeType nodeType = NodeType.Open;

	/// <summary>
	/// Grid point represented by node
	/// </summary>
	public Field point;

	public double _priority;

	public override double Priority
	{
		get { return finalCost; }
		set { _priority = value; Debug.Log("Unexpected priority set"); }
	}

	public static MapSearchNode Create(Field point, Field target, MapSearchNode parent = null, NodeType nodeType = NodeType.Open)
	{
		MapSearchNode n = new MapSearchNode();
		n.point = point;
		n.nodeType = nodeType;
		n.parent = parent;
		n.SetEstimatedCost(target);
		return n;
	}
}