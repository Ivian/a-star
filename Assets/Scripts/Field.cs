﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class Field : MonoBehaviour, IPointerClickHandler
{
	private SpriteRenderer spriteRenderer;

	private Color defaultColor = Color.white;

	public Point pointInArray;
	public Point coord;

	private void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		SetDefaultColor();
	}

	public FlagMask<FieldState> walkability = new FlagMask<FieldState>();


	public bool hasPlayer = false;

	public bool isTarget = false;


	public void OnPointerClick(PointerEventData eventData)
	{
		if (eventData.button == PointerEventData.InputButton.Right)
			ToggleWalkability();
		else
		{
			isTarget = true;
            Map.instance.FieldSelected(this);
			SetDefaultColor();
		}
	}

	public void SetDefaultColor()
	{
		if (hasPlayer)
			defaultColor = Color.green;
		else if (walkability.IsSet(FieldState.Unwalkable))
			defaultColor = Color.red;
		else if (isTarget)
			defaultColor = Color.yellow;
		else
			defaultColor = Color.white;

		if (walkability.IsSet(FieldState.Hidden))
			defaultColor *= 0.8f;

		defaultColor.a = 1f;

		spriteRenderer.color = defaultColor;
	}


	private void ToggleWalkability()
	{
		if (walkability.IsSet(FieldState.Unwalkable))
			walkability.UnsetFlag(FieldState.Unwalkable);
		else
			walkability.SetFlag(FieldState.Unwalkable);

		SetDefaultColor();
	}

	public void PlayerEnnteredTile()
	{
		hasPlayer = true;
		isTarget = false;
		RevealTile();
    }

	public void PlayerLeftTile()
	{
		hasPlayer = false;
		SetDefaultColor();
	}

	public void RevealTile()
	{
		walkability.UnsetFlag(FieldState.Hidden);
		SetDefaultColor();
	}

}
