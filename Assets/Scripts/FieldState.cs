﻿/// <summary>
/// FieldState for Fields of Map
/// </summary>
public enum FieldState
{
	Invalid = 1,    // Invalid. Returned if point extends the map size
	Unwalkable,     // Is set as unwalkable in tiles
	Untraceable,    // Is not able to be traced to
	Hidden,         // Is hidden in fog of war
	Blocked,        // Is blocked, because of enemy
	WalkableOpen,   // Is walkable, and marked as open
	WalkableClosed  // Is walkable, and marked as closed
}